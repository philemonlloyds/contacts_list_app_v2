require 'pg'
require 'pry'

puts 'Connecting to the database...'
begin
$conn = PG.connect(
  host: 'localhost',
  dbname: 'contactlist',
  user: 'development',
  password: 'development'
)

rescue PG::ConnectionBad
  puts "Sorry there is somthing wrong with your credentials."
end

puts "Connected to the Database"