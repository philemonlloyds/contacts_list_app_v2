class Contact

  attr_reader :id, :first_name, :last_name,:email

  def initialize(id, first_name, last_name,email)
    @id = id
    @first_name = first_name
    @last_name = last_name
    @email = email
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.all
    pg_result = $conn.exec_params('SELECT * FROM contacts')
    to_array(pg_result)
  end

  def self.search(last_name)
    pg_result = $conn.exec_params("SELECT * FROM contacts WHERE lastname = $1", [last_name])
    to_array(pg_result)
  end

  def save!
    pg_result = $conn.exec_params("INSERT INTO contacts (firstname, lastname,email) VALUES ('#{first_name}', '#{last_name}', '#{email}')")
  end

  private
  def self.to_array(pg_result)
    result = []
    pg_result.each do |row|
      # row {"id"=>"1213", "last_name"=>"Brookins", "first_name"=>"Andrew"}
      result << Contact.new(row["id"], row["firstname"], row["lastname"],row["email"])
    end
    result
  end

end