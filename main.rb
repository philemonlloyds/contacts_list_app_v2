require_relative 'connect'
require_relative 'contact'

# puts 'Finding contacts in the app...'
# $conn.exec('SELECT * FROM contacts;') do |results|
#   results.each do |contact|
#     puts contact.inspect
#   end
# end

def list
  contacts = Contact.all 
  contacts.each do |contact|
    # puts "ID: #{contact.id} First_Name: #{contact.first_name} Last_Name:#{contact.last_name}"
    puts "ID: #{contact.id} Full_Name: #{contact.full_name}"
  end
end

def search
  print "Enter a last name: "
  last_name = gets.chomp
  contacts = Contact.search(last_name)
  contacts.each do |contact|
    puts "ID: #{contact.id} Full Name: #{contact.full_name}"
  end
end

def create
  print "Enter first name: "
  first_name = gets.chomp
  print "Enter last name: "
  last_name = gets.chomp
  print "Enter email: "
  email = gets.chomp

  # Create a new row in memory
  contact = Contact.new(nil,first_name,last_name,email)
  # Save the row to the database
  contact.save!
end

puts "Lighthouse ORM"

loop do
  print %q(
Menu

list - show all contacts
create - create a new contact
search - find an contact by last name
update - update contacts's last name
quit - exit program
  > )

  command = gets.chomp

  case command.downcase
  when "list"
    list
  when "create"
    create
  when "search"
    search
  when "update"
    update
  when "quit"
    puts "Closing connection"
    $conn.close
    break
  end
end

puts 'DONE'